<?php

include "../libcommon/conf.php";
include "../libcommon/classes/db_mysql.php";
include "../libcommon/functions.php";
include "../libcommon/db_inc.php";

include "header.php";
	// print_r($_POST);
	if ($_SERVER['REQUEST_METHOD'] == "POST") 
	{
		$first_name 		= trim(sql_real_escape_string($_POST['first_name']));
		$middle_name 		= trim(sql_real_escape_string($_POST['middle_name']));
		$family_name 		= trim(sql_real_escape_string($_POST['family_name']));
		$enrolment_date 	= trim(sql_real_escape_string($_POST['enrolment_date']));
		$dob				= trim(sql_real_escape_string($_POST['dob']));
		
		$email 				= trim(sql_real_escape_string($_POST['email']));
		$password 			= trim(sql_real_escape_string($_POST['password']));
		$cnfm_password 		= trim(sql_real_escape_string($_POST['cnfm_password']));
		$mobile 			= trim(sql_real_escape_string($_POST['mobile']));
		$home_phone 		= trim(sql_real_escape_string($_POST['home_phone']));
		$myimage 			= trim(sql_real_escape_string($_POST['photoimage']));
		$parent1_relation 	= trim(sql_real_escape_string($_POST['parent1_relation']));
		$parent1_email 		= trim(sql_real_escape_string($_POST['parent1_email']));
		$parent1_mobile 	= trim(sql_real_escape_string($_POST['parent1_mobile']));
		$parent2_relation 	= trim(sql_real_escape_string($_POST['parent2_relation']));
		$parent2_email 		= trim(sql_real_escape_string($_POST['parent2_email']));
		$parent2_mobile 	= trim(sql_real_escape_string($_POST['parent2_mobile']));
		$emergency_mobile 	= trim(sql_real_escape_string($_POST['emergency_mobile']));
		$emergency_email	= trim(sql_real_escape_string($_POST['emergency_email']));
		$back_pain 			= trim(sql_real_escape_string($_POST['back_pain']));
		$allergies 			= trim(sql_real_escape_string($_POST['allergies']));

		$address_street_no 	    = trim(sql_real_escape_string($_POST['address_street_no']));
		$address_street_name 	= trim(sql_real_escape_string($_POST['address_street_name']));
		$address_postcode 		= trim(sql_real_escape_string($_POST['address_postcode']));
		$address_suburb 		= trim(sql_real_escape_string($_POST['address_suburb']));
		$address_state 			= trim(sql_real_escape_string($_POST['address_state']));
		$parent1_name 			= trim(sql_real_escape_string($_POST['parent1_name']));
		$parent2_name 			= trim(sql_real_escape_string($_POST['parent2_name']));
		$emergency_contact_name = trim(sql_real_escape_string($_POST['emergency_contact_name']));
		$other_medical_pbm 		= trim(sql_real_escape_string($_POST['other_medical_pbm']));


		$passwordmd5 = md5($password);
		// $cnfm_password = md5('$cnfm_password');
		$res = sql_query("LOCK TABLES student WRITE", $connect);
		$query = "insert into student (first_name,middle_name,family_name,enrolment_date,dob,email,password,mobile,home_phone,studentImage,parent1_relation,parent1_email,parent1_mobile,parent2_relation,parent2_email,parent2_mobile,emergency_mobile,emergency_email,back_pain,allergies,address_street_no,address_street_name,address_suburb,address_state,parent1_name,parent2_name,emergency_contact_name,other_medical_pbm,address_postcode) values ('$first_name','$middle_name','$family_name',STR_TO_DATE('$enrolment_date', '%d-%m-%Y'),STR_TO_DATE('$dob', '%d-%m-%Y'),'$email','$passwordmd5','$mobile','$home_phone','$myimage','$parent1_relation','$parent1_email','$parent1_mobile','$parent2_relation','$parent2_email','$parent2_mobile','$emergency_mobile','$emergency_email','$back_pain','$allergies','$address_street_no','$address_street_name','$address_suburb','$address_state','$parent1_name','$parent2_name','$emergency_contact_name','$other_medical_pbm','$address_postcode')";

		$result = sql_query($query,$connect);

		

		if (sql_error($res)) 
		{
			if(trim(sql_error()) != '') 
				$input_errors[] =  "Failed to save user: ".$first_name." ".sql_error_report(trim(mysql_errno()));
		}

		if( $input_errors) 
		{
			input_error_reporting($input_errors);
			echo "<form action='save_detail.php' method='POST' id='edit_details'>";
			include_once 'enter_details.php';
			echo "</form>";
		}
		else
		{
			$student_id = sql_query("SELECT LAST_INSERT_ID()", $connect);
	    	$row = sql_fetch_row($student_id);
	    	$id = $row[0];

	    	$res = sql_query("UNLOCK TABLES", $connect);
			if ($id != 0) {
				//header("Location: select_studio.php?id=".$id); 
				echo " <script type=\"text/javascript\">
			window.location.href=\"select_studio.php?id=".$id."\";
		    </script>";
				exit();
			}
			else
			{
				echo "<form action='save_detail.php' method='POST' id='edit_details'>";
				include_once 'enter_details.php';	
				echo "</form>";
			}

		}
	}
	


?>