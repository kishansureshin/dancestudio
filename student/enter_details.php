<script type="text/javascript" src="../libcommon/calendar/ui/ui.core.js"></script>
<script type="text/javascript" src="../libcommon/calendar/ui/ui.datepicker.js"></script>
<script type="text/javascript" src="../libcommon/javascripts/ajaxupload.js"></script>
<link href="../libcommon/calendar/themes/all.css" rel="stylesheet" type="text/css" />
<script src="../libcommon/javascripts/jquery.validate.js"></script>
<style type="text/css">
	
	td {font-weight: bold;}

</style>
<script>
	
$(document).ready(function() {


		Materialize.updateTextFields();
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    // yearRange: '1980:2013',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false,
    format: 'dd-mm-yy' // Close upon selecting a date,
  });

	$("#edit_details").validate({
		rules : {
			password : {
				required : true,
				minlength : 6
			},
			cnfm_password : {
				required : true,
				minlength : 6,
				equalTo : "#password"
			},
			mobile : {
				required : true,
				minlength : 10
			}

		}
  		
 		// $("#edit_details").submit();
 	});

	// $(".datepicker").datepicker({dateFormat: 'dd-mm-yy'});		


    var button = $('#upload_image'), interval;
	var fileUpload = new AjaxUpload(button,
	{
        action: 'uploaded_photo.php', 						
        name: 'doc',
        onSubmit : function(file, ext)
        {						
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext)))
            {
                // extension is not allowed
                alert('Error: invalid file extension');
                // cancel upload
                return false;
            }	
            
            button.text('Uploading');
            this.disable();
            interval = window.setInterval(function()
	        {
	            var text = button.text();
	            if (text.length < 13)
    	        {
	                button.text(text + '.');					
	            } 
	            else 
	            {
	                button.text('Uploading');				
	            }
	        }, 200);
        },
        onComplete: function(file, response)
        {
	        button.html("<p style=color:#F00; font-size:12px;>Successfully Uploaded</p>");							
	        window.clearInterval(interval);											
	        this.enable();				
	        $('#imgshow').html(response);						
        }
    });
    
});	
function removePhoto(url,imgurl)
	{			
		var dataString = "imgurl="+imgurl;
		//alert(dataString);
		$.ajax({
	        type: "GET",
	        url: url,
	        data: dataString,
	        success: function(response)
	        {
	            $('#imgshow').html(response);
	        }
	    });
	    return false;			
	}

</script>

<div class="container">
<div class="row">
<div class="col s10 offset-s2">
			<blockquote>
      			<h5>Student Information</h5>
    		</blockquote>
			<div class="input-field col s5">
		  		
		  		<i class="material-icons prefix">account_circle</i>
		          <input type='text' size='40' placeholder="first name" id="first_name" name='first_name' value='<?=$first_name?>' style="width:250px" minlength="2" required>
		          <label for="icon_prefix">First Name</label>
          	</div>

          	<div class="input-field col s5">
		  		
		  		<i class="material-icons prefix">account_circle</i>
		         <input type='text' size='40' id="middle_name" placeholder="middle name" name='middle_name' value='<?=$middle_name?>' style="width:250px">
		          <label for="icon_prefix">Middle Name</label>
          	</div>

          	<div class="input-field col s5">
		  		
		  		<i class="material-icons prefix">account_circle</i>
		          <input type='text' size='40' name='family_name' placeholder="family name" value='<?=$family_name?>' style="width:250px">
		          <label for="icon_prefix">Family Name</label>
          	</div>

          	<div class="input-field col s5">
		  		
          		
		  		<?php
					if ($enrolment_date) 
					{

						// echo date("d-m-Y", strtotime($enrolment_date));
						echo '<i class="material-icons prefix">date_range</i>';
						echo '<input type="text"  size="40" name="enrolment_date" placeholder="enrolment date" value="'.date("d-m-Y", strtotime($enrolment_date)).'" style="width:250px" readonly="true" >';
						echo '<label for="icon_prefix">Enrolment Date</label>';
					}
					else
					{
						echo '<i class="material-icons prefix">date_range</i>';
						echo '<input type="text" class="datepicker" size="40" name="enrolment_date" placeholder="enrolment date" value="" style="width:250px" required>';

						echo '<label for="icon_prefix">Enrolment Date</label>';
					}
				?>

		  		
		         
		         
          	</div>
          	<?
				if ($dob) {
					$dateofbirth = date("d-m-Y", strtotime($dob));
				}
				else
				{
					$dateofbirth = "";	
				}
				
			?>
          	<div class="input-field col s5">
		  		
		  		<i class="material-icons prefix">date_range</i>
		          <input required type='text' size='40' class="datepicker" name='dob' placeholder="date of birth" value='<?=$dateofbirth?>' style="width:250px">
		          <label for="icon_prefix">Date of birth</label>
          	</div>




<div class="input-field col s10">
     <blockquote>
      	<h6>Address</h6>
    </blockquote>
    </div>

     <div class="input-field col s5">
  		<i class="material-icons prefix">home</i>
          <input type="text"  class="input-large mandatory regx_general" placeholder="Street No" name='address_street_no' id='address_street_no' value='<?echo $address_street_no?>'>
          <label for="icon_prefix">Street No</label>
     </div>

     <div class="input-field col s5">
  		<i class="material-icons prefix">streetview</i>
          <input type="text"  class="input-large mandatory regx_name" placeholder="Street Name" name='address_street_name' id='address_street_name' value='<?echo $address_street_name?>'>
          <label for="icon_prefix">Street Name</label>
     </div>

     <div class="input-field col s5">
  		<i class="material-icons prefix">streetview</i>
          <input type="text"  class="input-large mandatory regx_name"  placeholder="Suburb" name='address_suburb' id='address_suburb' value='<?echo $address_suburb?>'>
          <label for="icon_prefix">Suburb</label>
     </div>

     <div class="input-field col s5">
  		<i class="material-icons prefix">domain</i>
          <input type="text"  class="input-large mandatory regx_name"  placeholder="State" name='address_state' id='address_state' value='<?echo $address_state?>'>
          <label for="icon_prefix">State</label>
     </div>

     <div class="input-field col s5">
  		<i class="material-icons prefix">drafts</i>
          <input type="text"  class="input-large mandatory regx_name"  placeholder="Post Code" name='address_postcode' id='address_postcode' value='<?echo $address_postcode?>'>
          <label for="icon_prefix">Postcode</label>
     </div>

      <div class="input-field col s10">
     <blockquote>
      	<h6>Other</h6>
    </blockquote>
    </div>


    <div class="input-field col s5">
  		<i class="material-icons prefix">email</i>
          <input type='email' required size='40' name='email' placeholder="email" value='<?=$email?>' style="width:250px">
          <label for="icon_prefix">Email</label>
     </div>

     <?
		if (!$update_flag) 
		{
		?>
		<div class="input-field col s5">
  		<i class="material-icons prefix">security</i>
		<input type='password' data-rule-minlength="6" id="password" required size='40' name='password' placeholder="password" value='' style="width:250px">
		<label for="icon_prefix">Password</label>
     	</div>		

		<div class="input-field col s5">
		<i class="material-icons prefix">security</i>
		<input type='password' id="cnfm_password" data-rule-minlength="6" required size='40' name='cnfm_password' placeholder="confirm password" value='' style="width:250px">
		<label for="icon_prefix">Confirm Password</label>
     	</div>
		<?}
		else
		{?>


		<input type='hidden' data-rule-minlength="6" id="password" required size='40' name='password' placeholder="password" value='<?=$password?>' style="width:250px">

		<input type='hidden' id="cnfm_password" data-rule-minlength="6" required size='40' name='cnfm_password' placeholder="confirm password" value='<?=$cnfm_password?>' style="width:250px">

		<?
		}
		?>


		<div class="input-field col s5">
  		<i class="material-icons prefix">phone_android</i>
          <input type='text' size='40' name='mobile' placeholder="mobile number" value='<?=$mobile?>'>
          <label for="icon_prefix">Mobile</label>
     </div>


     <div class="input-field col s5">
  		<i class="material-icons prefix">phone</i>
          <input type='text' size='40' name='home_phone' placeholder="home phone" value='<?=$home_phone?>' style="width:250px">
          <label for="icon_prefix">Home Phone</label>
     </div>
          <div class="input-field col s10">
     	<?php		
function defaultimage()
{
	echo "	
		<div id=\"imgshow\">
		<img src=\"../libcommon/images/missing.png\" style=\"width:75px; height:80px; float:left;margin-left:51px;padding:10px;\" class='materialboxed' width='87' /> 
		<div id=\"upload_image\">
		<input name=\"upload_image\" class=\"btn\" style=\"margin:40px 0 0 30px;\" type=\"button\" value=\"Upload Image\"  />
		</div>
		<input name=\"photoimage\" type=\"hidden\" value=\"\" />
		<span id=\"file\"></span>
		</div>";
}				
if( $_GET[action] == "frm_input" || $_GET[action] == "input")
{
	defaultimage();
}
else 
{
	// echo $myimage;
	if($myimage == NULL)
	{
		defaultimage();
	}
	else 
	{
		if(file_exists("".$myimage.""))
		{
			echo "<div id=\"imgshow\">
				<img src=".$myimage." style=\"width:75px; height:80px; float:left;margin-left:51px;padding:10px;\"/>
				<input name=\"photoimage\" type=\"hidden\" value=".$myimage." />
				<input type=\"button\" class=\"btn\" value=\"Remove\" style=\"margin:40px 0 0 30px;\" onClick=\"removePhoto('removephotoimage.php','".$myimage."')\" />
				</div>";
			// echo "<div id=\"imgshow\">
			// 	<img src=".$myimage." class='materialboxed' width='65' />
			// 	<input name=\"photoimage\" type=\"hidden\" value=".$myimage."  />
			// 	<input type=\"button\" class=\"btn\" value=\"Remove\" onClick=\"removePhoto('removephotoimage.php','".$myimage."')\" />
			// 	</div>";
		}
		else 
		{
			defaultimage();
		}
	}
}
?>
  
</div>


<div class="input-field col s10">
     <blockquote>
      	<h5>Parent/Guardian Information</h5>
    </blockquote>
    </div>

<div class="input-field col s5">
  		<i class="material-icons prefix">supervisor_account</i>
          <input type='text' size='40' required name='parent1_relation' placeholder="parent relation" value='<?=$parent1_relation?>' style="width:250px">
          <label for="icon_prefix">Relation</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">account_circle</i>
          <input type='text' size='40' name='parent1_name' placeholder="parent name" value='<?=$parent1_name?>' style="width:250px">
          <label for="icon_prefix">Name</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">email</i>
          <input type='email' size='40' name='parent1_email' placeholder="parent email" value='<?=$parent1_email?>' style="width:250px" required>
          <label for="icon_prefix">Email</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">phone_android</i>
          <input type='text' size='40' name='parent1_mobile' placeholder="parent mobile" value='<?=$parent1_mobile?>' style="width:250px" required >
          <label for="icon_prefix">Mobile</label>
     </div>

     <div class="input-field col s5">
  		<i class="material-icons prefix">supervisor_account</i>
          <input type='text' size='40' name='parent2_relation' placeholder="parent relation" value='<?=$parent2_relation?>' style="width:250px" required>
          <label for="icon_prefix">Relation</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">account_circle</i>
          <input type='text' size='40' name='parent2_name' placeholder="parent name" value='<?=$parent2_name?>' style="width:250px">
          <label for="icon_prefix">Name</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">email</i>
          <input type='email' size='40' name='parent2_email' placeholder="parent email" value='<?=$parent2_email?>' style="width:250px" required>
          <label for="icon_prefix">Email</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">phone_android</i>
          <input type='text' size='40' name='parent2_mobile' placeholder="parent mobile" value='<?=$parent2_mobile?>' style="width:250px" required>
          <label for="icon_prefix">Mobile</label>
     </div>

     <div class="input-field col s10">
     <blockquote>
      	<h5>Emergency Contact Details</h5>
    </blockquote>
    </div>

	<div class="input-field col s5">
  		<i class="material-icons prefix">account_circle</i>
          <input type='text' size='40' name='emergency_contact_name' placeholder="emergency contact name" value='<?=$emergency_contact_name?>' style="width:250px">
          <label for="icon_prefix">Emergency Contact Name</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">phone_android</i>
          <input type='text' size='40' name='emergency_mobile' placeholder="emergency mobile" value='<?=$emergency_mobile?>' style="width:250px" required>
          <label for="icon_prefix">Mobile No</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">email</i>
          <input type='email' size='40' name='emergency_email' placeholder="emergency email" value='<?=$emergency_email?>' style="width:250px" required>
          <label for="icon_prefix">Email</label>
     </div>

     <div class="input-field col s10">
     <blockquote>
      	<h5>Medical History</h5>
    </blockquote>
    </div>

	<div class="input-field col s5">
  		<i class="material-icons prefix">local_hotel</i>
          <input type='text' size='40' name='back_pain' placeholder="back pains, if any" value='<?=$back_pain?>' style="width:250px" >
          <label for="icon_prefix">Back Pain</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">sentiment_dissatisfied</i>
          <input type='text' size='40' name='allergies' placeholder="allergies, if any" value='<?=$allergies?>' style="width:250px" >
          <label for="icon_prefix">Allergies</label>
     </div>
     <div class="input-field col s5">
  		<i class="material-icons prefix">local_hospital</i>
          <input type='text' size='40' name='other_medical_pbm' placeholder="other, if any" value='<?=$other_medical_pbm?>' style="width:250px">
          <label for="icon_prefix">Other</label>
     </div>

     <?

     	if ($_SESSION['student_id']) {
		
			$student_id = $_SESSION['student_id'];
		      echo '<div class="input-field col s10">
		     <blockquote>
		      	<h5>Studios</h5>
		    </blockquote>
		    </div>';
    
			$query = "select stu.name,dt.type_name,le.level_name,loc.location_name,sr.time_from,sr.time_to from studio stu,dance_type dt,location loc,level le,studio_relation sr,student_studio_relation ssr where ssr.student_id = '$student_id' and ssr.studio_relation_id = sr.id and stu.id = sr.studio_id and dt.id=sr.dance_type_id and loc.id = location_id and le.id = sr.level_id;";
			$result = sql_query($query,$connect);
			if (sql_num_rows($result)) {
				while ($row = sql_fetch_array($result)) 
				{?>
				<div class="input-field col s5">
				<div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title"><?=$row['name']?></span>
              	<div>Type : <?=$row['type_name']?></div>
              	<div>Level : <?=$row['level_name']?></div>
              	<div>Location : <?=$row['location_name']?></div>
              	<div>Time from : <?=$row['time_from']?></div>
              	<div>Time to : <?=$row['time_to']?></div>
            </div>
           
          </div>
          </div>
          <?}
      		}
      	}
          ?>
          <div class="input-field col s12">
           <div class="input-field col s6">
           		<input type="submit" name="submit-btn" value="Save and Select Studio" class='btn'></input>
           </div>
          	<?
          		if (!$_SESSION['student_id']) {
          			
          	?>
          	<div class="input-field col s6">
          		<a href="../student/index.php"><input type="button" name="submit-btn" value="Cancel" class='btn'></input></a> 		
          	</div>
          	<?
          	}
          	?>
	
          	</div>

</div>
</div>
</div>



