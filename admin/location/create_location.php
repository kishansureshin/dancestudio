<style type="text/css">
    td {
        font-weight: bold;
    }
</style>
<script type="text/javascript">

function save_list_location(url, div, save)
{

    $("#pagination").html("");
        
    if(save == 1)
    {
        var error = $(".validation").jquery_validation("div", "validation");
        
        var loc_name = $("#loc_name").val();
        
        if(error == 1)
        {
            return false; 
        }
            
    }
    $('#loader').show();
    var dataString = "loc_name="+loc_name+"&save="+save;
    $.ajax({
            type: "POST",
            url: "location/"+url,
            data: dataString,
            success: function(response)
            {

                if(save == 1)
                {
                    $("#loc_name").val("");
                }
    
                $('#loader').hide(); 
                $('#'+div).html(response);

            }
          });  
            return false; 
}

function edit_location(id, slno, url)
{

    var dataString = "id="+id+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "location/"+url,
            data: dataString,
            success: function(response)
            {
                $('#loc_name_edit'+id).html(response);

            }
          });  
    return false; 
}

function update_location(id, slno, url)
{
    // var error = $(".validation").jquery_validation("table tr", "loc_name_row"+id);
    var location =  $("#loc_name"+id).val();
    // alert(dance_type);
    
    // if(error == 1)
    // {
    //     return false; 
    // }
    
    var dataString = "location_id="+id+"&loc_name="+location+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "location/"+url,
            data: dataString,
            success: function(response)
            {

                $('#loc_name_edit'+id).html(response);

            }
          });  
            
        return false;
}


function cancel_update(id, slno, url)
{
    var dataString = "id="+id+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "location/"+url,
            data: dataString,
            success: function(response)
            {
                $('#loc_name_edit'+id).html(response);

            }
          });  
            return false; 
    
}


function delete_location(id, name, url)
{
    //var num = $(".subjects").length;
    jConfirm('You are going to delete the dance type <span style="color:#F00; font-weight:bold;">'+name+'</span>. All the associated data will be lost!!', 'Confirmation', function(r) {
    if( r==true)
    {
        var dataString = "id="+id;
        $.ajax({
            type: "POST",
            url: "location/"+url,
            data: dataString,
            success: function(response)
            {
                if(response == 2)
                {
                    jAlert('<span style="font-size:12px; color:red;">Cant Delete. Operation Failed</span>', 'Warning');
                }
                else
                {
                    /*if(num ==1)
                    {
                        $("#listsubjects").html("<h2 style=\"text-align:center; margin:5% 5%; color:#F00;\">No Subjects Left</h2>");
                    }*/
                    //$('#subject'+id).hide();
                    save_list_location('ajax_save_list_location.php', 'listlocation', 0);
                }

            }
          });  
            return false; 
    }
    });
}


function callplugin(totaldata, current, data_per_page, pagelimit, url, divid, data)
{
    $("#pagination").ajaxpagination(totaldata, current, data_per_page, pagelimit, url, divid, data);
}  
    
</script>
<?
include "session.php";

?>


<div class="container">
<div class="row">
<div class="col s10 offset-s2">
            <blockquote>
                <h5>Create Location</h5>
            </blockquote>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">drag_handle</i>
     <input id='loc_name' type='text' size='30' class="required regx_general" style="text-transform:uppercase;" onblur="javascript:this.value=this.value.toUpperCase();">
    <label for="icon_prefix">Studio Location</label>
</div>

<div class="input-field col s5">

     <input name="upload" type="submit" class="btn" id="upload" value="Submit"  onclick="save_list_location('ajax_save_list_location.php', 'listlocation', 1);"  class="btn">
</div>


</div>
</div>
</div>


<div id="listlocation" class="listlocation">

    <div id="loader" style="width:100px;height:50px;margin-left:450px;margin-top:25px;float:left;display:none;"><img src="../libcommon/images/ajax_load.gif" /></div>

</div>

<div id="pagination" style="text-align:right;"></div>

<script type="text/javascript">
    save_list_location('ajax_save_list_location.php', 'listlocation', 0);
</script>

