<script type="text/javascript">
	
function mark_attendance(student_id,studio_id,date,day)
{
	var comment = $("#comment"+student_id+day).val();
	
	var isAbsent = 0
	
	if ($('#student_check'+student_id+day).is(':checked')) 
	{
		isAbsent = 0;
	}
	else
	{
		isAbsent = 1;
	}

	var dataString = "studio_id="+studio_id+"&student_id="+student_id+"&comment="+comment+"&date="+date+"&isAbsent="+isAbsent+"&day="+day;
	console.log(dataString);
	$.ajax({
		    type: "POST",
		    url: "attendance/ajax_mark_attendance.php",
		    data: dataString,
		    success: function(response)
		    {

		    	if (response.trim() == 1) 
		    	{
		    		Materialize.toast('Error Occured!', 3000, 'rounded')
		    	}
		    	else
		    	{
		    		Materialize.toast('Attendance updated successfully!', 3000, 'rounded')	
		    	}
		    }
	  	});  
	    
	    return false;
}

function create_attendance(url,div)
{
	var studio_id = $("#studio").val();
	var month = $("#month").val();
	var year = $("#year").val();
	var date = 	$("#date").val();

	var dataString = "studio_id="+studio_id+"&year="+year+"&month="+month+"&date="+date;
	// console.log(dataString);
	if (studio_id && month && year ) 
	{
			$.ajax({
		    type: "POST",
		    url: "attendance/"+url,
		    data: dataString,
		    success: function(response)
		    {
		    	$("#"+div).html(response);
		    }
	  	});  
	    
	    	return false;
	}
	else
	{
		Materialize.toast('Please select all fields!', 3000, 'rounded')
	}

	
}

function loadDates()
{
	var studio_id = $("#studio").val();
	var month = $("#month").val();
	var year = $("#year").val();

	var dataString = "studio_id="+studio_id+"&year="+year+"&month="+month;
	
	$.ajax({
	    type: "POST",
	    url: "attendance/ajax_load_dates.php",
	    data: dataString,
	    success: function(response)
	    {

	    $("#dates").html(response);
	                Materialize.updateTextFields();
            $('select').material_select();
	    }
  	});  
    
    return false;

}

</script>

<style type="text/css">
	
	.select-dropdown{
    overflow-y: auto !important;
}
</style>

<div class="container">
<div class="row">
<div class="col s10 offset-s2">
            <blockquote>
                <h5>Attendance System</h5>
            </blockquote>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">swap_vert</i>


    <?php
        $query = "select * from studio";
        $result = sql_query($query,$connect);
        if (sql_num_rows($result)) {
            echo "<select id='studio'>
                <option disabled selected value=''>Select</option>";
            while ($row = sql_fetch_array($result)) {
                echo "<option value='$row[id]'>".$row['name']."</option>";
            }
            echo "</select>";
        }

    ?>
    

    <label for="icon_prefix">Select Studio</label>
</div>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">dvr</i>
    <select id="month" onchange="loadDates();">
		<option value="">Select</option>
		<option value="1">January</option>
		<option value="2">February</option>
		<option value="3">March</option>
		<option value="4">April</option>
		<option value="5">May</option>
		<option value="6">June</option>
		<option value="7">July</option>
		<option value="8">August</option>
		<option value="9">September</option>
		<option value="10">October</option>
		<option value="11">Novermber</option>
		<option value="12">December</option>
	</select>

	<label for="icon_prefix">Month</label>
</div>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">dvr</i>
    <select id="year" onchange="loadDates();">
		<option value="">Select</option>
		<?
			$i = 2010;
			while ($i <= date('Y')) {
				echo "<option value='$i'>".$i."</option>";
				$i++;
			}
		?>
	</select>

	<label for="icon_prefix">Year</label>
</div>

<!-- <div class="input-field col s5 validation" id="dates">
    <i class="material-icons prefix">dvr</i>
    
    <select id="date">
		<option value="">Select</option>

	</select>
	
	<label for="icon_prefix">Select Date</label>
</div> -->

<div class="input-field col s5">

     <input name="upload" type="button" class="btn" id="upload" value="Create Attendance"  onclick="create_attendance('ajax_save_list_students.php', 'liststudents');"  class="btn">
</div>





</div>
</div>
</div>


<div id="liststudents" class="liststudents">

    <div id="loader" style="width:100px;height:50px;margin-left:450px;margin-top:25px;float:left;display:none;"><img src="../libcommon/images/ajax_load.gif" /></div>

</div>

<div id="pagination" style="text-align:right;"></div>


<script type="text/javascript">
	
 $(document).ready(function() 
    {
            Materialize.updateTextFields();
            $('select').material_select();
            
            
    });



</script>