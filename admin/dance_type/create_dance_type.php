<style type="text/css">
    td {
        font-weight: bold;
    }
</style>

<script type="text/javascript">



function save_list_dance_type(url, div, save)
{

    $("#pagination").html("");
        
    if(save == 1)
    {
        var error = $(".validation").jquery_validation("div", "validation");
        
        var type_name = $("#type_name").val();
        
        if(error == 1)
        {
            return false; 
        }
            
    }
    $('#loader').show();
    var dataString = "type_name="+type_name+"&save="+save;
    $.ajax({
            type: "POST",
            url: "dance_type/"+url,
            data: dataString,
            success: function(response)
            {

                if(save == 1)
                {
                    $("#type_name").val("");
                    Materialize.toast('Added successfully!', 3000, 'rounded')
                }
                
                $('#loader').hide(); 
                $('#'+div).html(response);

            }
          });  
            return false; 
}

function edit_dance_type(id, slno, url)
{

    var dataString = "id="+id+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "dance_type/"+url,
            data: dataString,
            success: function(response)
            {
                $('#type_name_edit'+id).html(response);

            }
          });  
    return false; 
}

function update_dance_type(id, slno, url)
{
    // var error = $(".validation").jquery_validation("table tr", "type_name_row"+id);
    var dance_type =  $("#type_name"+id).val();
    // alert(dance_type);
    
    // if(error == 1)
    // {
    //     return false; 
    // }
    
    var dataString = "dance_type_id="+id+"&type_name="+dance_type+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "dance_type/"+url,
            data: dataString,
            success: function(response)
            {

                $('#type_name_edit'+id).html(response);
                Materialize.toast('Updated successfully!', 3000, 'rounded')

            }
          });  
            
        return false;
}


function cancel_update(id, slno, url)
{
    var dataString = "id="+id+"&slno="+slno;

    $.ajax({
            type: "POST",
            url: "dance_type/"+url,
            data: dataString,
            success: function(response)
            {
                $('#type_name_edit'+id).html(response);

            }
          });  
            return false; 
    
}


function delete_dance_type(id, name, url)
{
    //var num = $(".subjects").length;

    jConfirm('You are going to delete the dance type <span style="color:#F00; font-weight:bold;">'+name+'</span>. All the associated data will be lost!!', 'Confirmation', function(r) {
    if( r==true)
    {
        var dataString = "id="+id;
        $.ajax({
            type: "POST",
            url: "dance_type/"+url,
            data: dataString,
            success: function(response)
            {
                if(response == 2)
                {
                    jAlert('<span style="font-size:12px; color:red;">Cant Delete. Operation Failed</span>', 'Warning');
                }
                else
                {
                    /*if(num ==1)
                    {
                        $("#listsubjects").html("<h2 style=\"text-align:center; margin:5% 5%; color:#F00;\">No Subjects Left</h2>");
                    }*/
                    //$('#subject'+id).hide();
                    save_list_dance_type('ajax_save_list_dance_type.php', 'listdancetype', 0);
                }

            }
          });  
            return false; 
    }
    });
}


function callplugin(totaldata, current, data_per_page, pagelimit, url, divid, data)
{
    $("#pagination").ajaxpagination(totaldata, current, data_per_page, pagelimit, url, divid, data);
}  
    
</script>
<?
include "session.php";

?>


<div class="container">
<div class="row">
<div class="col s10 offset-s2">
            <blockquote>
                <h5>Dance Type</h5>
            </blockquote>
<div class="input-field col s5 validation">
    <i class="material-icons prefix">drag_handle</i>
     <input id='type_name' type='text' size='30' class="required regx_general" style="text-transform:uppercase;" onblur="javascript:this.value=this.value.toUpperCase();">
    <label for="icon_prefix">Dance Type Name</label>
</div>

<div class="input-field col s5">

     <input name="upload" type="submit" class="btn" id="upload" value="Submit"  onclick="save_list_dance_type('ajax_save_list_dance_type.php', 'listdancetype', 1);" >
</div>

</div>
</div>
</div>


  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Delete Confirmation</h4>
      <p>Are you sure, you want to delete?</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">CANCEL</a>
    </div>
  </div>
          

<div id="listdancetype" class="listdancetype">

    <div id="loader" style="width:100px;height:50px;margin-left:450px;margin-top:25px;float:left;display:none;"><img src="../libcommon/images/ajax_load.gif" /></div>

</div>

<div id="pagination" style="text-align:right;"></div>

<script type="text/javascript">
    save_list_dance_type('ajax_save_list_dance_type.php', 'listdancetype', 0);
</script>

