<?php
session_start();
include "../../libcommon/conf.php";
include "../../libcommon/classes/sql.cls.php";
include "../../libcommon/classes/db_mysql.php";
include "../../libcommon/db_inc.php";
//include "../../session.php";
include "../../libcommon/functions.php";
    
    
    $start =$_POST["start"];
    $end =$_POST["end"];
    
    $type_name = trim(sql_real_escape_string($_POST["type_name"]));

    
    $save = trim(sql_real_escape_string($_POST["save"]));

    if($start || $end)                     //$start || $end is used becuse o is treated as null so limit 0 15 at first will not possible.
    {
        $condition = "limit ".$start.", ".$end;
    }
    else
    {
        $condition = "";
    }
    
    if($save ==1)
    {
        $sql = "INSERT INTO dance_type (type_name) VALUES ('$type_name')";
        $result = sql_query($sql, $connect);
    
        if(mysql_error())
        {
            $error = "$type_name ". sql_error_report(trim(mysql_errno()));
            ?>
            <script type="text/javascript">
                jAlert('<span style="font-size:12px; color:red;">Cant Create. <?echo $error;?>!!</span>', 'Warning');
            </script>
            <?
        }
    }
    
        $sql = "SELECT id,type_name FROM dance_type order by id desc $condition";
        // echo $sql;
        $result = sql_query($sql, $connect);
        if(sql_num_rows($result))
        {


            echo "<div class='container'><div class='row'><div class='col s10 offset-s2'><table class='bordered'>
				<tr >
                <th >Sl.No</th>
                <th >Dance Type</th>   
                <th >Edit</th>
                <th >Delete</th>
            
            </tr>";
            while($row = sql_fetch_array($result))
            {
                $dance_type_id = $row[0];
                echo "<tr align=\"center\" class=\"type_name_row".$dance_type_id."\" id=\"type_name_edit".$dance_type_id."\">
                <td>".(++$start)."</td>
                <td>".$row[1]."</td>

                <td><div class='btn-floating z-depth-2' onclick=\"edit_dance_type(".$row[0].", ".$start.", 'ajax_edit_dance_type.php');\"><i class='small material-icons white-text' >mode_edit</i></div></td>
                <td><div  class='red lighten-1 btn waves-effect btn-floating z-depth-2' onclick=\"delete_dance_type(".$row[0].", '".$row[1]."', 'ajax_delete_dance_type.php');\"><i class='small material-icons white-text' >delete</i></div></td>
                 </tr>";
            }
            echo"</table></div></div></div>";
        }
        else
        {
            echo "<h2 style=\"text-align:center; margin:5% 5%; color:#F00;\">No Dance Type Defined</h2>";
        }

    sql_logout($connect);
?>
          
