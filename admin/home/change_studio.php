



<script type="text/javascript">
	function save_studio(sid)
	{
		var studios = "";
		
		$( ".studio_name:checked" ).each(function() {
		    studios = studios+','+$( this ).val();
		});

		$.ajax
	    ({
	        url : "home/ajax_save_studio.php",
	        type : "POST",
	        data : 'studios='+studios+"&sid="+sid,
	        success : function(response)
	        {
	        	if (response.trim() == 1) 
	        	{
	        		jAlert(response+"<span style='color:red;'>Error occurred</span>");	
	        	}
	        	else
	        	{
	        		 jAlert(response+"<span style='color:blue;'>Saved Successfully</span>", 'Success', function(r) {
	                   if(r == true)
	                   {
	                       window.location.href="?u=home&b=sp";
	                   }
	                   });	
	        	}
	        	
	        },
	    });
	}


	function listStudio(sid)
	{
		var dance_type = "";
		var level = "";
		var location = "";

		$( ".dance_type_class:checked" ).each(function() {
		    dance_type = dance_type+','+$( this ).val();
		 });

		$( ".location_class:checked" ).each(function() {
		    location = location+','+$( this ).val();
		 });

		$( ".level_class:checked" ).each(function() {
		    level = level+','+$( this ).val();
		 });

		var dataString = "dance_type="+encodeURI(dance_type)+"&location="+encodeURI(location)+"&level="+encodeURI(level)+"&sid="+encodeURI(sid);

		$.ajax
	    ({
	        url : "home/ajax_list_studio.php",
	        type : "POST",
	        data : dataString,
	        success : function(response)
	        {
	            $('#list_studio').html(response);
	        },
	    });
	}

</script>

<div class="container">
<div class="row">
<div class="col s10 offset-s2">

	<blockquote>
      	<h5>Select Dance Type</h5>
      	
    </blockquote>
   
    <?php
	$query = "select id,type_name from dance_type";
	$result = sql_query($query,$connect);
	if (sql_num_rows($result)) 
	{
		
		// echo "<table class='formInput' style='text-align:left'>";
		while ($row = sql_fetch_array($result)) 
		{
			$id = $row['id'];
			$dance_type = $row['type_name'];
			
			echo "
			<div class='col s4'>
			<input class='dance_type_class' id='type".$id."' type='checkbox' value='".$id."' />
			<label for='type".$id."'>".$dance_type."</label></div>";
			
		}
		
	}
	else
	{
		echo "<h5 style='color:red;'>No dance type defned.</h5>";
	}
?>
	</div>
</div>
</div>
	<div class="container">
<div class="row">
<div class="col s10 offset-s2">

	<blockquote>
      	<h5>Select Location</h5>
    </blockquote>
    <?php
	$query = "select id,location_name from location";
	$result = sql_query($query,$connect);
	if (sql_num_rows($result)) 
	{

		while ($row = sql_fetch_array($result)) 
		{
			$id = $row['id'];
			$location = $row['location_name'];
			echo "<div class='col s4'>";
			echo "<input type='checkbox' class='location_class' id='location".$id."' value='$id'>
			<label for='location".$id."'>".$location."</label></div>";
			
		}
		
	}
	else
	{
		echo "<h5 style='color:red;'>No locations defned.</h5>";
	}
?>

</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="col s10 offset-s2">

	<blockquote>
      	<h5>Select Level</h5>
    </blockquote>
    <?php
	$query = "select id,level_name from level";
	$result = sql_query($query,$connect);
	if (sql_num_rows($result)) 
	{
		
		while ($row = sql_fetch_array($result)) 
		{
			$id = $row['id'];
			$level_name = $row['level_name'];

			echo "<div class='col s4'>";
			echo "<input type='checkbox' id='level".$id."' class='level_class' value='$id'>";
			echo "<label for='level".$id."'>".$level_name."</label></div>";
			
			
		}
		
	}
	else
	{
		echo "<h5 style='color:red;'>No levels type defned.</h5>";
	}
?>

</div>
</div>
</div>

<div class="container">
<div class="row">
<div class="col s10 offset-s2" style="padding: 20px;">
	<input type="button" class="btn" onclick="listStudio('<?php echo $_GET[id]; ?>');" value="Select Studio" style="cursor: pointer;text-align: center;"></input>
</div>
	</div></div>
<div id="list_studio"></div>

