<?

session_start();

if ($_GET['export'] == 1) {
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");;
	header("Content-Disposition: attachment;filename=student_report.xls ");
	header("Content-Transfer-Encoding: binary ");
}

include "../../libcommon/conf.php";
include "../../libcommon/classes/sql.cls.php";
include "../../libcommon/classes/db_mysql.php";
include "../../libcommon/db_inc.php";
//include "../../session.php";
include "../../libcommon/functions.php";


$studio_id = trim(sql_real_escape_string($_GET['studio']));
// $month = trim(sql_real_escape_string($_GET['month']));

if ($studio_id != "") {
	$condition = "and stu.id = '$studio_id'";

	$studioName = sql_fetch_array(sql_query("select name from studio where id = ".$studio_id,$connect))[0];
}
else
{
	$condition = '';
}

if ($_GET['studio_details'] == 1) {
	$query = "select st.*,stu.name as studio_name from student st,studio stu, studio_relation sr, student_studio_relation ssr where sr.id = ssr.studio_relation_id and st.id = ssr.student_id and sr.studio_id = stu.id and st.blocked = 0 $condition";
}
else
{
	$query = "select st.*,stu.name as studio_name from student st,studio stu, studio_relation sr, student_studio_relation ssr where sr.id = ssr.studio_relation_id and st.id = ssr.student_id and sr.studio_id = stu.id and st.blocked = 0 $condition";
}

$result = sql_query($query,$connect);

if (sql_num_rows($result)) 
{
	echo "<div class='container'><div class='row'><div class='col s10 offset-s2'><table class='bordered'>
		<tr><th colspan = '10'>".$studioName."</th></tr>
		<tr>
			<th style='width:10%;'>
				Sl No.
			</th>";
			++$col;
			$getStr = "";
			if ($_GET['first_name']) 
			{
				++$col;
				echo "<th style='width:10%;'>First Name</th>";
				$getStr = $getStr."&first_name=".$_GET['first_name'];
			}
			if ($_GET['family_name']) 
			{
				++$col;
				echo "<th style='width:10%;'>Family Name</th>";
				$getStr = $getStr."&family_name=".$_GET['family_name'];
			}
			if ($_GET['middle_name']) 
			{
				++$col;
				echo "<th style='width:10%;'>Middle Name</th>";
				$getStr = $getStr."&middle_name=".$_GET['middle_name'];
			}
			
			if ($_GET['enrolment_date']) 
			{
				++$col;
				echo "<th style='width:10%;'>Enrolment Date</th>";
				$getStr = $getStr."&enrolment_date=".$_GET['enrolment_date'];
			}
			if ($_GET['dob']) 
			{
				++$col;
				echo "<th style='width:10%;'>Date of Birth</th>";
				$getStr = $getStr."&=dob".$_GET['dob'];
			}
			
			if ($_GET['email']) 
			{
				++$col;
				echo "<th style='width:10%;'>Email</th>";
				$getStr = $getStr."&email=".$_GET['email'];
			}
			if ($_GET['emergency_mobile']) 
			{
				++$col;
				echo "<th style='width:10%;'>Emergency Mobile</th>";
				$getStr = $getStr."&emergency_mobile=".$_GET['emergency_mobile'];
			}
			if ($_GET['home_phone']) 
			{
				++$col;
				echo "<th style='width:10%;'>Home Phone</th>";
				$getStr = $getStr."&home_phone=".$_GET['home_phone'];
			}
			if ($_GET['mobile']) 
			{
				++$col;
				echo "<th style='width:10%;'>Mobile</th>";
				$getStr = $getStr."&mobile=".$_GET['mobile'];
			}
			if ($_GET['studio_details'] == 1) 
			{
				++$col;
				echo "<th style='width:10%;'>Studio</th>";
				$getStr = $getStr."&studio_details=".$_GET['studio_details'];
			}
			// if ($month != "" && $_GET['export']) {
			// 	++$col;
			// 	echo "<th style='width:10%;'>Week 1</th>";
			// 	++$col;
			// 	echo "<th style='width:10%;'>Week 2</th>";
			// 	++$col;
			// 	echo "<th style='width:10%;'>Week 3</th>";
			// 	++$col;
			// 	echo "<th style='width:10%;'>Week 4</th>";
			// }

		echo "</tr>";
			if ($month != "" && $_GET['export']) {
				echo "<tr><th colspan='$col' align='center'>".$month."</th></tr>";
			}
		
			while ($row = sql_fetch_array($result)) {
				echo "<tr>";
				echo "<td>".++$i."</td>";
				if ($_GET['first_name']) 
				{
					echo "<td>".$row['first_name']."</td>";
				}

				if ($_GET['family_name']) 
				{
					echo "<td>".$row['family_name']."</td>";
				}
				if ($_GET['middle_name']) 
				{
					echo "<td>".$row['middle_name']."</td>";
				}
				
				if ($_GET['enrolment_date']) 
				{
					echo "<td>".date("Y-m-d", strtotime($row['enrolment_date']))."</td>";
				}
				if ($_GET['dob']) 
				{
					echo "<td>".date("Y-m-d", strtotime($row['dob']))."</td>";
				}
				
				if ($_GET['email']) 
				{
					echo "<td style='text-transform: none'>".$row['email']."</td>";
				}
				if ($_GET['emergency_mobile']) 
				{
					echo "<td>".$row['emergency_mobile']."</td>";
				}
				if ($_GET['home_phone']) 
				{
					echo "<td>".$row['home_phone']."</td>";
				}
				if ($_GET['mobile']) 
				{
					echo "<td>".$row['mobile']."</td>";
				}
				if ($_GET['studio_details'] == 1) 
				{
					echo "<td>".$row['studio_name']."</td>";
				}

				echo "</tr>";	
		}
		if ($_GET['export'] != 1) {
			echo "<th colspan='".$col."' align='center'>
				<a href='home/ajax_show_report.php?export=1&studio=$studio_id&month=".$month."".$getStr."'><input type='button' class='btn' value='Export to Excel' ></a>
			</th>";
		}
		echo "</table></div></div></div>";	
}
else
{
	echo "<div class='container'><div class='row'><div class='col s10 offset-s2'><h5 style='color:red;'>No students present</h5></div></div></div>";
}


?>