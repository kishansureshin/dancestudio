<style type="text/css">
    td {
        font-weight: bold;
    }

    .select-dropdown{
    overflow-y: auto !important;
}
</style>
<script type="text/javascript">

function show_report(url, div)
{

    $("#pagination").html("");
   
    $('#loader').show();
    
    var checkstring = '';

    if($("#first_name").attr('checked'))
    {
        var first_name = $("#first_name").val();
        checkstring = checkstring+"&first_name="+first_name;
    }
    if($("#middle_name").attr('checked'))
    {
        var middle_name = $("#middle_name").val();
        checkstring = checkstring+"&middle_name="+middle_name;
    }
    if($("#family_name").attr('checked'))
    {
        var family_name = $("#family_name").val();
        checkstring = checkstring+"&family_name="+family_name;
    }
    if($("#enrolment_date").attr('checked'))
    {
        var enrolment_date = $("#enrolment_date").val();
        checkstring = checkstring+"&enrolment_date="+enrolment_date;
    }
    if($("#dob").attr('checked'))
    {
        var dob = $("#dob").val();
        checkstring = checkstring+"&dob="+dob;
    }
   
    if($("#email").attr('checked'))
    {
        var email = $("#email").val();
        checkstring = checkstring+"&email="+email;
    }
    if($("#mobile").attr('checked'))
    {
        var mobile = $("#mobile").val();
        checkstring = checkstring+"&mobile="+mobile;
    }
    if($("#home_phone").attr('checked'))
    {
        var home_phone = $("#home_phone").val();
        checkstring = checkstring+"&home_phone="+home_phone;
    }
    if($("#emergency_mobile").attr('checked'))
    {
        var emergency_mobile = $("#emergency_mobile").val();
        checkstring = checkstring+"&emergency_mobile="+emergency_mobile;
    }

    var studio = $("#studio").val();
    var month  = $("#month").val();

    if($("#studio_details").attr('checked'))
    {
        var dataString = "studio="+studio+"&month="+month+"&studio_details=1"+checkstring;    
    }
    else
    {
        var dataString = "studio="+studio+"&month="+month+"&studio_details=0"+checkstring;
    }
     

    $.ajax({
            type: "GET",
            url: "home/"+url,
            data: dataString,
            success: function(response)
            {
                $('#loader').hide(); 
                $('#'+div).html(response);

            }
          });  
    return false; 
    
}




function callplugin(totaldata, current, data_per_page, pagelimit, url, divid, data)
{
    $("#pagination").ajaxpagination(totaldata, current, data_per_page, pagelimit, url, divid, data);
}  
    
</script>
<?
include "session.php";

?>
<div class="container">
<div class="row">
<div class="col s10 offset-s2">
            <blockquote>
                <h5>Report</h5>
            </blockquote>


<div class="input-field col s5 validation">
    <i class="material-icons prefix">dvr</i>


    <?php
       $query = "select * from studio";
        $result = sql_query($query,$connect);
        if (sql_num_rows($result)) {
            echo "<select id='studio' style='width:264px;' class='required mandatory regx_digit'>
                <option value=''>Select</option>";
            while ($row = sql_fetch_array($result)) {
                echo "<option value='$row[id]'>".$row['name']."</option>";
            }
            echo "</select>";
        }
        else
        {
            echo "<span style='color:red;font-weight:bold;'>No studio found! </span>";
        }
    ?>
    

    <label for="icon_prefix">Select Studio</label>
</div>


<div class=" col s10 validation">
    <!-- <i class="material-icons prefix">dvr</i> -->


    <!-- <select id="month">
        <option value="">Select</option>
        <option value="January">January</option>
        <option value="February">February</option>
        <option value="March">March</option>
        <option value="April">April</option>
        <option value="May">May</option>
        <option value="June">June</option>
        <option value="July">July</option>
        <option value="August">August</option>
        <option value="September">September</option>
        <option value="October">October</option>
        <option value="November">Novermber</option>
        <option value="December">December</option>
    </select>

    <label for="icon_prefix">Select Month</label> -->
<!-- </div>
 -->





<div class="col s5 validation">
    <input type="checkbox" value="firs_name" id="first_name">
    <label for='first_name'>First Name</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="family_name" id="family_name">
    <label for='family_name'>Family Name</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="middle_name" id="middle_name">
    <label for='middle_name'>Middle Name</label> 
</div>

<div class="col s5 validation">
    <input type="checkbox" value="enrolment_date" id="enrolment_date">
    <label for='enrolment_date'>Enrolment Date</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="dob" id="dob">
    <label for='dob'>Date of Birth</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="email" id="email">
    <label for='email'>Email Id</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="emergency_mobile" id="emergency_mobile">
    <label for='emergency_mobile'>Emergency Contact</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="mobile" id="mobile">
    <label for='mobile'>Mobile</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="home_phone" id="home_phone">
    <label for='home_phone'>Home Phone</label> 
</div>
<div class="col s5 validation">
    <input type="checkbox" value="studio_details" id="studio_details">
    <label for='studio_details'>Studio Details</label> 
</div>





<div class="input-field col s5 validation">
    <input name="upload" type="submit" class="btn" id="upload" value="Submit"  onclick="show_report('ajax_show_report.php', 'listlevel');" >
</div>

</div>
</div>
</div>

    
    

<div id="listlevel" class="listlevel">

    <div id="loader" style="display:none;width:100px;height:50px;margin-left:450px;margin-top:25px;float:left;"><img src="../libcommon/images/ajax_load.gif" /></div>

</div>

<div id="pagination" style="text-align:right;"></div>

<script type="text/javascript">
    $(document).ready(function() 
    {
            Materialize.updateTextFields();
            $('select').material_select();
            
    });
</script>

