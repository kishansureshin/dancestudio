<script type="text/javascript">
	function change_status(student_id,status)
	{
		$.ajax({
            type: "POST",
            url: "home/ajax_change_status.php",
            data: "status="+status+"&student_id="+student_id,
            success: function(response)
            {
                if (response.trim() == 1) 
                {
                	jAlert("<span style='color:red;'>Some error occurred.</span>");
                }
                else
                {
                	jAlert('Status updated successfully', 'Success', function(r) {
                   if(r == true)
                   {
                       window.location.reload();
                   }
                   });
                }
            }
          });  
    return false;
	}
</script>



<div class="container">
<div class="row">
<div class="col s10 offset-s2">
<?

	$query = "select id,first_name,blocked from student";
	$res = sql_query($query,$connect);

	if (sql_num_rows($res)) {
		echo "
		<table class='responsive-table bordered'>
		<tr>
			<th>
				Sl No.
			</th>
			<th>
				Student Name
			</th>
			<th>
				Edit
			</th>
			<th>
				Block
			</th>
		</tr>";
		while ($row = sql_fetch_array($res)) {
			echo "<tr>
				<td>
					".++$i."
				</td>
				<td>
					<b>".$row['first_name']."</b>
				</td>
				<td>
					<a class='btn-floating' href='?u=home&b=es&id=".$row['id']."'><i class='small material-icons white-text'>mode_edit</i></a>
				</td>
				<td>";
					if ($row['blocked'] == 1) {
						echo "<button onclick='change_status($row[id],0);' class='btn waves-effect btn z-depth-2'>Unblock</button>";
						// echo "<input type='button' value='Unblock' onclick='change_status($row[id],0);'>";
					}
					else
					{
						echo "<div onclick='change_status($row[id],1);' class='red lighten-1 btn waves-effect btn-floating z-depth-2'><i class='material-icons white-text'>block</i>Block</div>";
						// echo "<input type='button' value='Block' onclick='change_status($row[id],1);'>";
					}
					
				echo "</td>
			</tr>";
		}
		echo "</table>";
	}
	else
	{
		echo "<h2 style='color:red;'>No students available</h2>";
	}

?>

</div>
</div>
</div>

