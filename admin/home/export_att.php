<?

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");;
	header("Content-Disposition: attachment;filename=student_report.xls ");
	header("Content-Transfer-Encoding: binary ");



	include "../../libcommon/conf.php";
	include "../../libcommon/classes/sql.cls.php";
	include "../../libcommon/classes/db_mysql.php";
	include "../../libcommon/db_inc.php";
	//include "../../session.php";
	include "../../libcommon/functions.php";



	$studio_id = trim(sql_real_escape_string($_GET["studio_id"]));
	$year = trim(sql_real_escape_string($_GET["year"]));
	$month = trim(sql_real_escape_string($_GET["month"]));
	$date = trim(sql_real_escape_string($_GET["date"]));

	$studioName = sql_fetch_array(sql_query("select name from studio where id = ".$studio_id,$connect))[0];

$query = "SELECT day_id from studio_relation where studio_id = '$studio_id'";
$day_id = sql_fetch_array(sql_query($query,$connect))[0];

$days = getDays($year,$month,$day_id);



	$query = "select st.*,stu.name as studio_name from student st,studio stu, studio_relation sr, student_studio_relation ssr where sr.id = ssr.studio_relation_id and st.id = ssr.student_id and sr.studio_id = stu.id and st.blocked = 0 and stu.id = '$studio_id'";

$result = sql_query($query,$connect);

if (sql_num_rows($result)) 
{
	echo "
	
	<table class='bordered'>
	<tr>
		<th colspan = '7'>".$studioName."</th>
	</tr>
		<tr>
			<th>
				Sl No.
			</th>
			<th>
				First Name
			</th>
			<th>
				Middle Name
			</th>
			<th>
				Family Name
			</th>
			<th>
				Email
			</th>";
			foreach ($days as $day) {
				
			
				echo "<th>
					".$year."-".$month."-".$day."
				</th>
				<th>
				Comment
			</th>";

			}

			echo "</tr>";

			while ($row = sql_fetch_array($result)) 
			{
				

				$query = "SELECT student_id, attendance_date, studio_id, comment FROM student_attendance WHERE student_id = '$row[id]' AND studio_id = '$studio_id' AND attendance_date = '$date'";
				$res = sql_query($query,$connect);
				if (sql_num_rows($res)) {
					$checkbox_str = "checked";
					$comment = sql_fetch_array($res)['comment'];
 				}
 				else
 				{
 					$checkbox_str = "";
 					$comment = "";
 				}

				echo "<tr>
					<td>".++$i."</td>
					<td>".$row['first_name']."</td>
					<td>".$row['middle_name']."</td>
					<td>".$row['family_name']."</td>
					<td>".$row['email']."</td>";

					foreach ($days as $day) {
						
						$date = strtotime($day."-".$month."-".$year);

						$query = "SELECT student_id, attendance_date, studio_id, comment FROM student_attendance WHERE student_id = '$row[id]' AND studio_id = '$studio_id' AND attendance_date = '$date'";
						$res = sql_query($query,$connect);
						if (sql_num_rows($res)) {
							$checkbox_str = "checked";
							$comment = sql_fetch_array($res)['comment'];
		 				}
		 				else
		 				{
		 					$checkbox_str = "";
		 					$comment = "";
		 				}

						echo "<td>";
						if ($checkbox_str == 'checked') {
							echo "present";
						}
						echo "</td>";

						echo "<td>
							".$comment."
							</td>";						
						}

				echo "</tr>";
			}
			
			echo "</table>";	


}
else
{
	
}

function getDays($y,$m,$d){ 
    $date = "$y-$m-01";
    
    $first_day = date('N',strtotime($date));
    $first_day = $d - $first_day;

    $last_day =  date('t',strtotime($date));
    $days = array();
    for($i=$first_day; $i<=$last_day; $i=$i+7 ){
        if ($i > 0) {
        	$days[] = $i;
        }
        
    }

    return  $days;
}


?>