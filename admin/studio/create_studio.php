<style type="text/css">
    td {
        font-weight: bold;
    }

    .select-dropdown{
        overflow-y: auto !important;
    }
</style>

<script type="text/javascript">

function save_list_studio(url, div, save)
{

    $("#pagination").html("");
        
    if(save == 1)
    {
        var error = $(".validation").jquery_validation("div", "validation");
        
        var studio_name = $("#studio_name").val();
        var location_id = $("#location").val();
        var level_id = $("#level").val();
        var dance_type_id = $("#dance_type").val();
        var timeFrom = $("#timeFrom").val();
        var timeTo = $("#timeTo").val();
        var day_id = $("#day").val();
    
        
        if(error == 1)
        {
            return false; 
        }
            
    }
    $('#loader').show();
    var dataString = "studio_name="+studio_name+"&location_id="+location_id+"&level_id="+level_id+"&dance_type_id="+dance_type_id+"&save="+save+"&timeFrom="+timeFrom+"&timeTo="+timeTo+"&day_id="+day_id;
    
    // console.log(day_id);
    
    $.ajax({
            type: "POST",
            url: "studio/"+url,
            data: dataString,
            success: function(response)
            {
                if(save == 1)
                {
                    $("#studio_name").val("");
                    $("#dance_type").val("");
                    $("#location").val("");
                    $("#level").val("");
                    $("#timeFrom").val("");
                    $("#timeTo").val("");
                    $("#day").val("");
                }
    
                $('#loader').hide(); 
                $('#'+div).html(response);

            }
          });  
            return false; 
}

function edit_studio(id, slno, url)
{

    var dataString = "id="+id+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "studio/"+url,
            data: dataString,
            success: function(response)
            {
                $('#studio_name_edit'+id).html(response);

            }
          });  
    return false; 
}

function update_studio(id, url)
{
    // var error = $(".validation").jquery_validation("table tr", "loc_name_row"+id);
    var studio_name =  $("#studio_name").val();
    var location_id = $("#location").val();
    var level_id = $("#level").val();
    var dance_type_id = $("#dance_type").val();
    var timeFrom = $("#timeFrom").val();
    var timeTo = $("#timeTo").val();
    var day_id = $("#day").val();
    // alert(dance_type);
    
    // if(error == 1)
    // {
    //     return false; 
    // }
    
    var dataString = "studio_name="+studio_name+"&location_id="+location_id+"&level_id="+level_id+"&dance_type_id="+dance_type_id+"&timeFrom="+timeFrom+"&timeTo="+timeTo+"&id="+id+"&day_id="+day_id;
    
    $.ajax({
            type: "POST",
            url: "studio/"+url,
            data: dataString,
            success: function(response)
            {
            	// jAlert(response);
            	save_list_studio('ajax_save_list_studio.php', 'liststudio', 0);
                window.location.href = '?u=studio&b=stu';

            }
          });  
            
        return false;
}


function cancel_update(id, slno, url)
{
    var dataString = "id="+id+"&slno="+slno;
    $.ajax({
            type: "POST",
            url: "location/"+url,
            data: dataString,
            success: function(response)
            {
                $('#loc_name_edit'+id).html(response);

            }
          });  
            return false; 
    
}


function delete_studio(id, name, url)
{
    //var num = $(".subjects").length;
    jConfirm('You are going to delete the dance studio <span style="color:#F00; font-weight:bold;">'+name+'</span>. All the associated data will be lost!!', 'Confirmation', function(r) {
    if( r==true)
    {
        var dataString = "id="+id;
        $.ajax({
            type: "POST",
            url: "studio/"+url,
            data: dataString,
            success: function(response)
            {
                if(response == 2)
                {
                    jAlert('<span style="font-size:12px; color:red;">Cant Delete. Operation Failed</span>', 'Warning');
                }
                else
                {
                    /*if(num ==1)
                    {
                        $("#listsubjects").html("<h2 style=\"text-align:center; margin:5% 5%; color:#F00;\">No Subjects Left</h2>");
                    }*/
                    //$('#subject'+id).hide();
                    save_list_studio('ajax_save_list_studio.php', 'liststudio', 0);
                }

            }
          });  
            return false; 
    }
    });
}


function callplugin(totaldata, current, data_per_page, pagelimit, url, divid, data)
{
    $("#pagination").ajaxpagination(totaldata, current, data_per_page, pagelimit, url, divid, data);
}  
    
</script>
<?
include "session.php";

?>

<?
    $studio_relation_id = trim(sql_real_escape_string($_GET["id"]));
    $slno = $_POST["slno"];
    
    echo $sql = "select st.name,sr.location_id,sr.level_id,sr.dance_type_id,sr.time_from,sr.time_to,sr.day_id from studio st,studio_relation sr where sr.studio_id = st.id and sr.id = '$studio_relation_id'";
    $result = sql_query($sql, $connect);
    $row = sql_fetch_array($result);
    
    $studio_name = $row[0];
    $location_id = $row[1];
    $level_id = $row[2];
    $dance_type_id = $row[3];
    $time_from = $row[4];
    $time_to = $row[5];
    $day_id = $row[6];


    $day_select[$day_id] = "selected";
    $location_select[$location_id] = "selected";
    $level_select[$level_id] = "selected";
    $dance_type_select[$dance_type_id] = "selected";

?>
<div class="container">
<div class="row">
<div class="col s10 offset-s2">
            <blockquote>
                <h5>Create Studio</h5>
            </blockquote>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">drag_handle</i>
     <input id='studio_name' type='text' size='30' value="<?=$studio_name?>" class="required" style="text-transform:uppercase;" onblur="javascript:this.value=this.value.toUpperCase();">
    <label for="icon_prefix">Studio Name</label>
</div>


<div class="input-field col s5 validation">
    <i class="material-icons prefix">dvr</i>


    <?php
        $query = "select * from dance_type";
        $result = sql_query($query,$connect);
        if (sql_num_rows($result)) {
            echo "<select id='dance_type' class='validate input-field col s12'>
                <option disabled selected value=''>Select</option>";
            while ($row = sql_fetch_array($result)) {
                echo "<option ".$dance_type_select[$row[id]]." value='$row[id]'>".$row['type_name']."</option>";
            }
            echo "</select>";
        }
        else
        {
            echo "<span style='color:red;font-weight:bold;'>No dance type found! <a href='?u=dance_type&b=dt'>Create dance type</a></span>";
        }
    ?>
    

    <label for="icon_prefix">Select Dance Type</label>
</div>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">add_location</i>


   <?php
        $query = "select * from location";
        $result = sql_query($query,$connect);
        if (sql_num_rows($result)) {
            echo "<select class='input-field col s12' id='location' style='width:264px;'>
                <option disabled selected value=''>Select</option>";
            while ($row = sql_fetch_array($result)) {
                echo "<option ".$location_select[$row[id]]." value='$row[id]'>".$row['location_name']."</option>";
            }
            echo "</select>";
        }
        else
        {
            echo "<span style='color:red;font-weight:bold;'>No locations found! <a href='?u=location&b=loc'>Create location</a></span>";
        }
    ?>
    

    <label for="icon_prefix">Select Location</label>
</div>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">swap_vert</i>


    <?php
        $query = "select * from level";
        $result = sql_query($query,$connect);
        if (sql_num_rows($result)) {
            echo "<select id='level' style='width:264px;' class='input-field col s12'>
                <option disabled selected value=''>Select</option>";
            while ($row = sql_fetch_array($result)) {
                echo "<option ".$level_select[$row[id]]." value='$row[id]'>".$row['level_name']."</option>";
            }
            echo "</select>";
        }
        else
        {
            echo "<span style='color:red;font-weight:bold;'>No level found! <a href='?u=level&b=le'>Create level</a></span>";
        }
    ?>
    

    <label for="icon_prefix">Select Level</label>
</div>



<div class="input-field col s5 validation">
    <i class="material-icons prefix">access_time</i>
        <input class="timepicker" name="timeFrom" type="text" id="timeFrom" value="<?=$time_from;?>" readonly="true" />
    <label for="icon_prefix">Time From</label>
</div>


<div class="input-field col s5 validation">
    <i class="material-icons prefix">access_time</i>
     <input class="timepicker" name="timeTo"  type="text" id="timeTo" value="<?=$time_to;?>" />
    <label for="icon_prefix">Time To</label>
</div>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">swap_vert</i>


    <?php
        
        
            echo "<select id='day' style='width:264px;' class='input-field col s12'>
                <option disabled selected value=''>Select</option>";
            foreach ($day as $day_key => $day_value) 
            {
                echo "<option ".$day_select[$day_key]." value='$day_key'>".$day_value."</option>";
            }
            echo "</select>";
        
    ?>
    

    <label for="icon_prefix">Select day</label>
</div>

<div class="input-field col s10">

    <?php 

        if ($_GET['id']) {
            echo '<input name="upload" type="submit" class="btn" id="upload" value="Update"  onclick=\'update_studio('.$studio_relation_id.', "ajax_update_studio.php");\' >&nbsp;
                <a href="?u=studio&b=stu"><input name=\'upload\' type=\'submit\' class=\'btn\' id=\'cancel\' value=\'Cancel\' ></a>';
        }
        else
        {
            echo "<input name=\"upload\" type=\"submit\" class=\"btn\" id=\"upload\" value=\"Submit\"  onclick=\"save_list_studio('ajax_save_list_studio.php', 'liststudio', 1);\" >";
        }

    ?>

     
</div>

</div>
</div>
</div>



<div id="liststudio" class="liststudio">

    <div id="loader" style="width:100px;height:50px;margin-left:450px;margin-top:25px;float:left;display:none;"><img src="../libcommon/images/ajax_load.gif" /></div>

</div>

<div id="pagination" style="text-align:right;"></div>

<script type="text/javascript">
    
    save_list_studio('ajax_save_list_studio.php', 'liststudio', 0);
    $(document).ready(function() 
    {
            Materialize.updateTextFields();
            $('select').material_select();


      $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function(){} //Function for after opening timepicker
      });
            
            
    });

</script>

