<style type="text/css">
    td {
        font-weight: bold;
    }
</style>
<script type="text/javascript">


	$(document).ready(function() 
    {
            Materialize.updateTextFields();
            $('select').material_select();
            
    });


	function showStudents(studio_relation_id)
	{
		var month = $("#month").val();
		var year = $("#year").val();
		if (month == "" || year == "") 
		{
			jAlert("<span style='color:red;'>Please choose both fields</span>");
			return false;
		}
		else
		{
			var dataString = "month="+month+"&year="+year+"&studio_relation_id="+studio_relation_id;
			$.ajax({
            type: "POST",
            url: "studio/ajax_show_student.php",
            data: dataString,
            success: function(response)
            {

            $("#studentslist").html(response);

            }
          	});  
            return false; 	
		}
	}

	function save_student_fee_detail(student_id,studio_relation_id)
	{
		var month = $("#month").val();
		var year = $("#year").val();
		if (month == "" || year == "") 
		{
			jAlert("<span style='color:red;'>Please choose both fields</span>");
			return false;
		}
		else
		{
			var no_of_week = $("#no_of_week"+student_id).val();
			var comment = $("#comment"+student_id).val();

			var action = $("#hide"+student_id).val();
			// if (action == "insert") 
			// {
			// 	var action = "insert";
			// }
			// else
			// {
			// 	var action = "delete";
			// }

			var dataString = "month="+month+"&year="+year+"&no_of_week="+no_of_week+"&comment="+comment+"&studio_relation_id="+studio_relation_id+"&student_id="+student_id+"&action="+action;
			

			$.ajax({
            type: "POST",
            url: "studio/ajax_save_student_fees.php",
            data: dataString,
            success: function(response)
            {

            	if (response.trim() == 1) 
            	{
            		jAlert("<span style='color:red;'>Some error has occured.</span>");
            	}
            	else
            	{
            		showStudents(studio_relation_id);
            	}
            	

            }
          	});  
            return false; 
		}
	}
</script>



<div class="container">
<div class="row">
<div class="col s10 offset-s2">
            <blockquote>
                <h5> <? 
   					echo $_GET['stu_name'];
   				?></h5>
            </blockquote>



<div class="input-field col s5 validation">
    <i class="material-icons prefix">dvr</i>
    <select id="month">
		<option value="">Select</option>
		<option value="January">January</option>
		<option value="February">February</option>
		<option value="March">March</option>
		<option value="April">April</option>
		<option value="May">May</option>
		<option value="June">June</option>
		<option value="July">July</option>
		<option value="August">August</option>
		<option value="September">September</option>
		<option value="October">October</option>
		<option value="November">Novermber</option>
		<option value="December">December</option>
	</select>

	<label for="icon_prefix">Month</label>
</div>

<div class="input-field col s5 validation">
    <i class="material-icons prefix">dvr</i>
    <select id="year">
		<option value="">Select</option>
		<?
			$i = 2010;
			while ($i <= date('Y')) {
				echo "<option value='$i'>".$i."</option>";
				$i++;
			}
		?>
	</select>

	<label for="icon_prefix">Year</label>
</div>



<div class="input-field col s5">
     <input name="upload" type="submit" class="btn" id="upload" value="Submit" onclick="showStudents(<?=$studio_relation_id?>);" >
</div>

<div id="studentslist" class="input-field col s10">
    <div id="loader" style="width:100px;height:50px;margin-left:450px;margin-top:25px;float:left;display:none;"><img src="../libcommon/images/ajax_load.gif" /></div>
</div>

</div>
</div>
</div>
