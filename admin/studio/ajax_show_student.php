<?

session_start();
include "../../libcommon/conf.php";
include "../../libcommon/classes/sql.cls.php";
include "../../libcommon/classes/db_mysql.php";
include "../../libcommon/db_inc.php";
//include "../../session.php";
include "../../libcommon/functions.php";


$studio_relation_id = trim(sql_real_escape_string($_POST['studio_relation_id']));
$month = trim(sql_real_escape_string($_POST['month']));
$year = trim(sql_real_escape_string($_POST['year']));


$query = "select st.id,st.first_name from student st, student_studio_relation ssr where st.id = ssr.student_id and ssr.studio_relation_id = '$studio_relation_id' and st.blocked = 0";
    	
        $result = sql_query($query,$connect);
    	
        if (sql_num_rows($result)) {
    		echo "<table class='bordered'>
    			<tr>
    				<th style='width:15%;'>
    					Sl No.
    				</th>
    				<th style='width:25%;'>
    					Student name
    				</th>
    				
    				<th style='width:20%;'>
    					Weeks
    				</th>
    				<th style='width:30%;'>
    					Comment
    				</th>
    				<th style='width:10%;'>
    					Select
    				</th>
    			</tr>";
    			while ($row = sql_fetch_array($result)) {
    				$student_id = $row[id];
                    $query = "select id,no_of_weeks,comment from student_fee_details where student_id = '$student_id' and year = '$year' and month = '$month' and studio_relation_id = '$studio_relation_id'";
                    $resultFeeDetails = sql_query($query,$connect);
                    if (sql_num_rows($resultFeeDetails)) 
                    {
                        $fee_details = sql_fetch_array($resultFeeDetails);
                        $fee_details_sel[$fee_details['1']] = "selected";
                        echo "
                        <tr>
                            <td>
                                ".++$k."
                            </td>
                            <td>
                                ".$row['first_name']."
                            </td>
                            
                            <td>
                                <select id = 'no_of_week".$student_id."' class='validate input-field col s5'>
                                <option ".$fee_details_sel['1']." value='1'>1</option>
                                <option ".$fee_details_sel['2']." value='2'>2</option>
                                <option ".$fee_details_sel['3']." value='3'>3</option>
                                <option ".$fee_details_sel['4']." value='4'>4</option>
                                </select>
                            </td>
                            <td>
                                <input type='text' id='comment".$student_id."' value = '$fee_details[2]'>
                            </td>
                            <td>

                                <input type='hidden' id='hide".$student_id."' value='delete'>
                                <button onclick='save_student_fee_detail(".$student_id.",".$studio_relation_id.");' class='btn waves-effect btn z-depth-2' value='".$student_id."'>UnSelect</button>
                            </td>
                        </tr>
                    ";    
                    }
                    else
                    {
                       echo "
                        <tr>
                            <td>
                                ".++$k."
                            </td>
                            <td>
                                ".$row['first_name']."
                            </td>
                            
                            <td>
                                <select class='validate input-field col s5' id = 'no_of_week".$student_id."'>
                                <option value='1'>1</option>
                                <option value='2'>2</option>
                                <option value='3'>3</option>
                                <option value='4'>4</option>
                                </select>
                            </td>
                            <td>
                                <input type='text' id='comment".$student_id."' >
                            </td>
                            <td>
                                
                                <input type='hidden' id='hide".$student_id."' value='insert'>
                                <button onclick='save_student_fee_detail(".$student_id.",".$studio_relation_id.");' id='student".$student_id."' class='btn waves-effect btn z-depth-2' value='".$student_id."'>Select</button>

                                
                            </td>
                        </tr>
                    "; 
                    }
    				
    			}
                echo "</table>";
    	}
    	else
    	{
    		echo " <div class='container'>
            <div class='row'>
            <div class='col s10 offset-s2'><blockquote><h5 style='color:red;'>No students present</h5> </blockquote></div></div></div>";
    	}



?>

<script type="text/javascript">
    

    $(document).ready(function() 
    {
            Materialize.updateTextFields();
            $('select').material_select();
            
            
    });

</script>
