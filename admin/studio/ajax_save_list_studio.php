<?php
session_start();
include "../../libcommon/conf.php";
include "../../libcommon/classes/sql.cls.php";
include "../../libcommon/classes/db_mysql.php";
include "../../libcommon/db_inc.php";
//include "../../session.php";
include "../../libcommon/functions.php";
    
    
    $start =$_POST["start"];
    $end =$_POST["end"];
    
    $studio_name = trim(sql_real_escape_string($_POST["studio_name"]));
    $dance_type_id = trim(sql_real_escape_string($_POST["dance_type_id"]));
    $location_id = trim(sql_real_escape_string($_POST["location_id"]));
    $level_id = trim(sql_real_escape_string($_POST["level_id"]));
    $timeFrom = trim(sql_real_escape_string($_POST["timeFrom"]));
    $timeTo = trim(sql_real_escape_string($_POST["timeTo"]));
    
    $save = trim(sql_real_escape_string($_POST["save"]));

    $day_id = trim(sql_real_escape_string($_POST["day_id"]));

    if($start || $end)                     //$start || $end is used becuse o is treated as null so limit 0 15 at first will not possible.
    {
        $condition = "limit ".$start.", ".$end;
    }
    else
    {
        $condition = "";
    }
    
    if($save ==1)
    {
        $res = sql_query("LOCK TABLES studio WRITE", $connect);
        $sql = "INSERT INTO studio (name) VALUES ('$studio_name')";
        $result = sql_query($sql, $connect);
        $res_studio_id = sql_query("SELECT LAST_INSERT_ID()", $connect);
        $row = sql_fetch_row($res_studio_id);
        $studio_id = $row[0];
        $res = sql_query("UNLOCK TABLES", $connect);

        $query = "insert into studio_relation (studio_id,dance_type_id,location_id,level_id,time_from,time_to,day_id) values ('$studio_id','$dance_type_id','$location_id','$level_id','$timeFrom','$timeTo','$day_id')";
        sql_query($query,$connect);

        if(mysql_error())
        {
            $error = "$studio_name ". sql_error_report(trim(mysql_errno()));
            ?>
            <script type="text/javascript">
                jAlert('<span style="font-size:12px; color:red;">Cant Create. <?echo $error;?>!!</span>', 'Warning');
            </script>
            <?
        }
    }
    
        $sql = "select sr.id,stu.name,dt.type_name,le.level_name,loc.location_name,sr.time_from,sr.time_to,stu.id from studio stu,dance_type dt,location loc,level le,studio_relation sr where stu.id = sr.studio_id and dt.id=sr.dance_type_id and loc.id = location_id and le.id = sr.level_id";
        // echo $sql;
        $result = sql_query($sql, $connect);
        if(sql_num_rows($result))
        {

            echo "<div class='container'><div class='row'><div class='col s10 offset-s2'><table class='responsive-table bordered'>
				<tr>
                <th>Sl.No</th>
                <th>Studio Name</th>
                <th>Dance Type</th>
                <th>Location</th>
                <th>Level</th>
                <th>Time From</th>
                <th>Time To</th>
                <th>Add Fee Details</th>   
                <th>Edit</th>
                <th>Delete</th>
            
            </tr>";
            while($row = sql_fetch_array($result))
            {
                $studio_relation_id = $row[0];
                echo "<tr align=\"center\" class=\"studio_name_row".$location_id."\" id=\"studio_name_edit".$studio_relation_id."\">
                <td>".(++$start)."</td>
                <td>".$row[1]."</td>
                <td>".$row[2]."</td>
                <td>".$row[4]."</td>
                <td>".$row[3]."</td>
                <td>".$row[5]."</td>
                <td>".$row[6]."</td>
                <td style='text-align:center;'>


                <a href=\"?u=studio&b=fee_details&id=".$studio_relation_id."&stu_name=".$row[1]."\">
                        <div class='btn-floating z-depth-2 blue darken-3'  ><i class='small material-icons white-text' >add</i></div>

                </a>

                </td>
                <td>
                <a href='?u=studio&b=stu&id=".$studio_relation_id."'><div class='btn-floating z-depth-2' ><i class='small material-icons white-text' >mode_edit</i></div></a>

                </td>
                <td>
                     <div class='red lighten-1 btn waves-effect btn-floating z-depth-2' onclick=\"delete_studio(".$row[0].", '".$row[1]."', 'ajax_delete_studio.php');\"><i class='small material-icons white-text' >delete</i></div>

                </td>
                 </tr>";
            }
            echo"</table></div></div></div>";
        }
        else
        {
            echo "<h2 style=\"text-align:center; margin:5% 5%; color:#F00;\">No Dance Type Defined</h2>";
        }

    sql_logout($connect);
?>
          
