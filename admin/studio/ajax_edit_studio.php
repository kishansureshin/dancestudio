<?php
session_start();
include "../../libcommon/conf.php";
include "../../libcommon/classes/sql.cls.php";
include "../../libcommon/classes/db_mysql.php";
include "../../libcommon/db_inc.php";
//include "../../session.php";
include "../../libcommon/functions.php";
?>

<script type="text/javascript" src="../libcommon/calendar/ui/ui.core.js"></script>
<script type="text/javascript" src="../libcommon/calendar/ui/ui.datepicker.js"></script>
<link href="../libcommon/calendar/themes/all.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../libcommon/clockpick/jquery.clockpick.js"></script>
<link href="../libcommon/clockpick/clockpick.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() 
{
        $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});        
    function cback() {
        this.after("<span id='cbox' />");
        $("#cbox")
        .text('')
        .addClass('red')
        .fadeIn(100)
        .fadeOut(1500);
    }
        
    $("select").not("#test").change(function() {
        binder();
    });
    
    function binder() {
        var opts = {};
        $("select").not("#test").each(function() {
            if ($(this).attr("id") != 'event' && $(this).attr("id") != 'layout') {
                opts[$(this).attr("id")] = eval($(this).val());
            }
            else {
                opts[$(this).attr("id")] = $(this).val();
            }
        }); 
        
        opts.valuefield = 'timeFrom';
        $(".timeFrom").unbind().unbind("keydown").clockpick(opts, cback);
        opts.valuefield = 'timeTo';
        $(".timeTo").unbind().unbind("keydown").clockpick(opts, cback);
    }
     binder();    
});
</script>


<?php

    $studio_relation_id = trim(sql_real_escape_string($_POST["id"]));
    $slno = $_POST["slno"];
    
    $sql = "select st.name,sr.location_id,sr.level_id,sr.dance_type_id,sr.time_from,sr.time_to from studio st,studio_relation sr where sr.studio_id = st.id and sr.id = '$studio_relation_id'";
    $result = sql_query($sql, $connect);
    $row = sql_fetch_array($result);
    
    $studio_name = $row[0];
    $location_id = $row[1];
    $level_id = $row[2];
    $dance_type_id = $row[3];
    $time_from = $row[4];
    $time_to = $row[5];
    $location_select[$location_id] = "selected";
    $level_select[$level_id] = "selected";
    $dance_type_select[$dance_type_id] = "selected";
    


    echo'<td colspan="9">


        <table class="formInput" cellpadding="0" cellspacing="3" border="0" align="center">
        <tr>
            <th colspan="2" align="center">Create Studio</th>
        </tr>
        <tr>
            <td width="35%">Studio Name<span style="color:#F00">*</span></td>
            <td><input id="studio_name'.$studio_relation_id.'" type="text" value="'.$studio_name.'" size="30" class="required regx_general" style="text-transform:uppercase;" onblur="javascript:this.value=this.value.toUpperCase();"></td>
        </tr>
        <tr>
            <td width="35%">Select dance type<span style="color:#F00">*</span></td>
            <td>';
               
                    $query = "select * from dance_type";
                    $result = sql_query($query,$connect);
                    if (sql_num_rows($result)) {
                        echo '<select id="dance_type'.$studio_relation_id.'" style="width:264px;" class="required mandatory regx_digit">
                            <option value="">Select</option>';
                        while ($row = sql_fetch_array($result)) {
                            echo "<option ".$dance_type_select[$row[id]]." value='$row[id]'>".$row['type_name']."</option>";
                        }
                        echo "</select>";
                    }
                    else
                    {
                        echo "<span style='color:red;font-weight:bold;'>No dance type found! <a href='?u=dance_type&b=dt'>Create dance type</a></span>";
                    }
            
           echo "</td>
        </tr>
        <tr>
            <td width='35%'>Select location<span style='color:#F00'>*</span></td>
            <td>";
                
                    $query = "select * from location";
                    $result = sql_query($query,$connect);
                    if (sql_num_rows($result)) {
                        echo "<select class='required mandatory regx_digit' id='location".$studio_relation_id."' style='width:264px;'>
                            <option value=''>Select</option>";
                        while ($row = sql_fetch_array($result)) {
                            echo "<option ".$location_select[$row[id]]." value='$row[id]'>".$row['location_name']."</option>";
                        }
                        echo "</select>";
                    }
                    else
                    {
                        echo "<span style='color:red;font-weight:bold;'>No locations found! <a href='?u=location&b=loc'>Create location</a></span>";
                    }
                

            echo "</td>
        </tr>
        <tr>
            <td width='35%'>Select level<span style='color:#F00'>*</span></td>
            <td>";
                    
                
                    $query = "select * from level";
                    $result = sql_query($query,$connect);
                    if (sql_num_rows($result)) {
                        echo "<select id='level".$studio_relation_id."' style='width:264px;' class='required mandatory regx_digit'>
                            <option value=''>Select</option>";
                        while ($row = sql_fetch_array($result)) {
                            echo "<option ".$level_select[$row[id]]." value='$row[id]'>".$row['level_name']."</option>";
                        }
                        echo "</select>";
                    }
                    else
                    {
                        echo "<span style='color:red;font-weight:bold;'>No level found! <a href='?u=level&b=le'>Create level</a></span>";
                    }
               


            echo '</td>
        </tr>
        <tr>
            <td width="35%">Time from<span style="color:#F00">*</span></td>
            <td>
                
                <input style="width: 264px;" class="timeFrom" name="timeFrom" type="text" id="timeFrom'.$studio_relation_id.'" value="'.$time_from.'" readonly="true" />

            </td>
        </tr>
        <tr>
            <td width="35%">Time to<span style="color:#F00">*</span></td>
            <td>
                
                <input name="timeTo" style="width: 264px;" class="timeTo" type="text" id="timeTo'.$studio_relation_id.'" value="'.$time_to.'" readonly="true" />

            </td>
        </tr>
        
        <tr>
            <th colspan="2" align="center"><input name="upload" type="submit" class="box" id="upload" value="Update"  onclick=\'update_studio('.$studio_relation_id.', '.$slno.', "ajax_update_studio.php");\' >&nbsp;<input name=\'upload\' type=\'submit\' class=\'box\' id=\'cancel\' value=\'Cancel\' onclick=\'save_list_studio("ajax_save_list_studio.php", "liststudio", 0);\' ></th>
        </tr>
    </table>


    </td>';
    

    sql_logout($connect);


?>