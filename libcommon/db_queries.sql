-- may 27, 2017

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `family_name` varchar(30) NOT NULL,
  `enrolment_date` date NOT NULL,
  `dob` date NOT NULL,
  `address` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `home_phone` bigint(20) NOT NULL,
  `studentImage` varchar(100) NOT NULL,
  `parent1_relation` varchar(30) NOT NULL,
  `parent1_email` varchar(50) NOT NULL,
  `parent1_mobile` bigint(20) NOT NULL,
  `parent2_relation` varchar(30) NOT NULL,
  `parent2_email` varchar(50) NOT NULL,
  `parent2_mobile` int(11) NOT NULL,
  `emergency_mobile` bigint(20) NOT NULL,
  `emergency_email` varchar(50) NOT NULL,
  `back_pain` varchar(100) NOT NULL,
  `allergies` varchar(100) NOT NULL,
  `studio_relation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB;


CREATE TABLE `dance_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


CREATE TABLE `studio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


CREATE TABLE `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


CREATE TABLE `studio_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studio_id` int(11) NOT NULL,
  `dance_type_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB; 


CREATE TABLE `student_studio_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `studio_relation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;


ALTER TABLE student_studio_relation ADD CONSTRAINT uniq_key UNIQUE (student_id,studio_relation_id);


CREATE TABLE `student_leave` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `student_id` INT NULL,
  `date_from` DATE NULL,
  `date_to` DATE NULL,
  PRIMARY KEY (`id`),
  INDEX `foreign_student_idx` (`student_id` ASC),
  CONSTRAINT `foreign_student`
    FOREIGN KEY (`student_id`)
    REFERENCES `student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION) ENGINE=InnoDB;


ALTER TABLE `student_leave` 
ADD COLUMN `matter` VARCHAR(45) NULL AFTER `date_to`;

CREATE TABLE `forgot_password` (
  `forgetID` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `random_number` varchar(50) NOT NULL,
  PRIMARY KEY (`forgetID`)
) ENGINE=InnoDB;


CREATE TABLE `student_fee_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `month` varchar(20) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `no_of_weeks` int(11) DEFAULT NULL,
  `comment` varchar(150) DEFAULT NULL,
  `studio_relation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_student_fee_details_1_idx` (`student_id`),
  KEY `fk_student_fee_details_2_idx` (`studio_relation_id`),
  CONSTRAINT `fk_student_fee_details_2` FOREIGN KEY (`studio_relation_id`) REFERENCES `studio_relation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_fee_details_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

/***********************kishan , july 19 *********************************/


ALTER TABLE `student` 
ADD COLUMN `parent1_name` VARCHAR(45) NULL AFTER `studio_relation_id`,
ADD COLUMN `parent2_name` VARCHAR(45) NULL AFTER `parent1_name`,
ADD COLUMN `emergency_contact_name` VARCHAR(45) NULL AFTER `parent2_name`,
ADD COLUMN `address_street_no` VARCHAR(45) NULL AFTER `emergency_contact_name`,
ADD COLUMN `address_street_name` VARCHAR(45) NULL AFTER `address_street_no`,
ADD COLUMN `address_suburb` VARCHAR(45) NULL AFTER `address_street_name`,
ADD COLUMN `address_state` VARCHAR(45) NULL AFTER `address_suburb`;


ALTER TABLE `student` 
DROP COLUMN `address`;


ALTER TABLE `student` 
CHANGE COLUMN `parent1_name` `parent1_name` VARCHAR(45) NULL DEFAULT NULL AFTER `studentImage`,
CHANGE COLUMN `parent2_name` `parent2_name` VARCHAR(45) NULL DEFAULT NULL AFTER `parent1_mobile`,
CHANGE COLUMN `emergency_contact_name` `emergency_contact_name` VARCHAR(45) NULL DEFAULT NULL AFTER `parent2_mobile`;


ALTER TABLE `student` 
ADD COLUMN `other_medical_pbm` VARCHAR(45) NULL AFTER `allergies`;


ALTER TABLE `student` 
ADD COLUMN `address_postcode` VARCHAR(45) NULL AFTER `address_state`;


ALTER TABLE `student` 
ADD COLUMN `blocked` INT(11) NULL DEFAULT 0 AFTER `address_postcode`;


/********************************kishan, september 29, 2017 ************************/

CREATE TABLE `notification_history` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `student_id` INT NULL,
  `changed_field` VARCHAR(45) NULL,
  `changed_field_value` VARCHAR(150) NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `notification_history` 
ADD COLUMN `student_field` VARCHAR(45) NULL AFTER `changed_field_value`;

ALTER TABLE `studio_relation` 
ADD COLUMN `day_id` INT NULL AFTER `time_to`;


CREATE TABLE `student_attendance` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `student_id` INT NOT NULL,
  `attendance_date` DATE NULL,
  `studio_id` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));


ALTER TABLE `student_attendance` 
ADD COLUMN `comment` VARCHAR(150) NULL AFTER `studio_id`;


ALTER TABLE `student_attendance` 
CHANGE COLUMN `attendance_date` `attendance_date` VARCHAR(50) NULL DEFAULT NULL ;


ALTER TABLE `student` 
ADD COLUMN `enrolment` INT NULL DEFAULT 0 AFTER `blocked`,
ADD COLUMN `skirt` INT NULL DEFAULT 0 AFTER `enrolment`,
ADD COLUMN `jacket` INT NULL DEFAULT 0 AFTER `skirt`;


/************************************kishan, november 22, 2017***************************/

ALTER TABLE `student` 
CHANGE COLUMN `mobile` `mobile` VARCHAR(50) NOT NULL ;
ALTER TABLE `student` 
CHANGE COLUMN `home_phone` `home_phone` VARCHAR(50) NOT NULL ,
CHANGE COLUMN `parent1_mobile` `parent1_mobile` VARCHAR(50) NOT NULL ,
CHANGE COLUMN `parent2_mobile` `parent2_mobile` VARCHAR(50) NOT NULL ,
CHANGE COLUMN `emergency_mobile` `emergency_mobile` VARCHAR(50) NOT NULL ;




